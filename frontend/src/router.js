import Vue from "vue";
import Router from "vue-router";
Vue.useAttrs(Router);
export default new Router({
    mode: "history",
    routes: [
        {
            path:"/",
            alias: "tutorials",
            name: "tutorials",
            component : () => import("./components/tutorial-list.vue")
        },
        {
            path: "/tutorials/:id",
            name: "tutorial-details",
            component: () => import("./components/single-tutorial.vue")
        },
        {
            path :"/add",
            name: "add",
            component :() => import("./components/add-tutorial.vue")
        }
    ]
});